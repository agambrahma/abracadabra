package ranker

import collection.mutable.ListBuffer

case class Game(team1: String, score1: Int, team2: String, score2: Int) {
  override def toString = s"$team1: $score1 VS $team2: $score2"
}

object Ranker {
  def WinPoints = 3
  def LossPoints = 0
  def DrawPoints = 1

  def computePoints(game: Game): Map[String, Int] = {
    if (game.score1 > game.score2) {
      Map(game.team1 -> WinPoints, game.team2 -> LossPoints)
    } else if (game.score2 > game.score1) {
      Map(game.team1 -> LossPoints, game.team2 -> WinPoints)
    } else {
      Map(game.team1 -> DrawPoints, game.team2 -> DrawPoints)
    }
  }

  def computePointsGames(games: List[Game]): Map[String, Int] = {
    def updater(score: Int) = (v: Option[Int]) => v match {
      case None => Some(score)
      case Some(oldScore) => Some(oldScore + score)
    }

    games.foldLeft(Map.empty[String, Int].withDefault(_ => 0)) {
      (map, game) => {
        val result = computePoints(game)
        map
          .updatedWith(game.team1)(updater(result(game.team1)))
          .updatedWith(game.team2)(updater(result(game.team2)))
      }
    }
  }

  def getRanks(points: Map[String, Int]): List[(String, Int)] = {
    def comp(p1: (String, Int), p2: (String, Int)): Boolean = {
      val (t1, s1) = p1
      val (t2, s2) = p2

      if (s1 != s2) {
        s1 > s2
      } else {
        t1 < t2
      }
    }

    points
      .toSeq
      .sortWith(comp)
      .toList
  }

  def showRanks(ranks: List[(String, Int)]): List[String] = {
    def ptsStr(s: Int): String =
      if (s == 1) {
        "1 pt"
      } else {
        s"$s pts"
      }

    val lines = ListBuffer[String]()
    var lastScore = -1
    var lastRank = -1

    val zippedRanks = ranks.zip(Range(1, ranks.size + 1))
    for (((t,s), rank) <- zippedRanks) {
      if (s != lastScore) {
        lastScore = s
        lastRank = rank
        lines.addOne(s"$rank. $t, ${ptsStr(s)}")
      } else {
        lines.addOne(s"$lastRank. $t, ${ptsStr(lastScore)}")
      }
    }

    lines.toList
  }
}

