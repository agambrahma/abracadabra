import ranker._

import scala.io.StdIn
import scala.collection.mutable.ListBuffer

object Main extends App {
  def readGames(): List[Game] = {
    val GamePattern = "(.+) (\\d+), (.+) (\\d+)".r
    var line = ""
    val gameList = ListBuffer[Game]()

    while ({line = StdIn.readLine(); line != null}) {
      line match {
        case GamePattern(t1, s1, t2, s2) => gameList.addOne(Game(t1, s1.toInt, t2, s2.toInt))
        case _ => println(s"Invalid line `$line`, skipping...")
      }
    }

    gameList.toList
  }

  // Retrieve games.
  val games = readGames()

  // Compute points.
  val points = Ranker.computePointsGames(games)

  // Sort and print.
  val ranks = Ranker.getRanks(points)
  val output = Ranker.showRanks(ranks)

  for (line <- output) println(line)
}
