import ranker._

import org.scalacheck.Properties
import org.scalacheck.Arbitrary
import org.scalacheck.Gen
import org.scalacheck.Prop._

import scala.util.Random
import scala.collection.mutable.ListBuffer

object RankerSpecification extends Properties("Ranker") {

  val teamList = (1 to 10).toList.map( (x) => s"team$x")
  val scoreList = 0 to 50

  implicit val arbitraryGame: Arbitrary[Game] = Arbitrary {
    for {
      teamName1 <- Gen.oneOf(teamList)
      teamName2 <- Gen.oneOf(teamList.filterNot( (x) => x == teamName1))

      teamScore1 <- Gen.oneOf(scoreList)
      teamScore2 <- Gen.oneOf(scoreList)
    } yield {
      Game(teamName1, teamScore1, teamName2, teamScore2)
    }
  }

  property("total_points") = forAll { (games: List[Game]) =>
    val points = Ranker.computePointsGames(games)
    val ranks = Ranker.getRanks(points)

    val totalPoints = ranks.foldLeft(0) {
      case (n, (_, i)) => n + i
    }

    val minBound = 2 * Ranker.DrawPoints * games.size
    val maxBound = Ranker.WinPoints * games.size

    (totalPoints <= maxBound) && (totalPoints >= minBound)
  }
}

class RankerTest extends org.scalatest.FunSuite {
  test("computePoints -> Win/Loss") {
    val result1 = Ranker.computePoints(Game("Foo", 4, "Bar", 2))
    assert(result1 === Map("Foo" -> 3, "Bar" -> 0))

    val result2 = Ranker.computePoints(Game("Alpha", 45, "Omega", 75))
    assert(result2 === Map("Alpha" -> 0, "Omega" -> 3))
  }

  test("computePoints -> Draw") {
    val result = Ranker.computePoints(Game("Red", 15, "Blue", 15))
    assert(result === Map("Red" -> 1, "Blue" -> 1))
  }

  test("computePointsGames -> Simple") {
    val game1 = Game("Foo", 10, "Bar", 5)
    val game2 = Game("Foo", 20, "Baz", 20)
    val result = Ranker.computePointsGames(List(game1, game2))

    assert(result === Map("Foo" -> 4, "Bar" -> 0, "Baz" -> 1))
  }

  test("computePointsGames -> Stress") {
    val teamNames = List.tabulate(20)(n => "Team # " + n)
    val numMatches = 500
    val rnd = new Random
    val games = ListBuffer.empty[Game]
    val expectedPoints = collection.mutable.Map[String, Int]().withDefault(_ => 0)

    for (_ <- 1 to numMatches) {
      import Helper.Outcome
      val (team1, team2) = Helper.getRandomTeams(rnd, teamNames)
      val randomOutcome = Helper.getRandomOutcome(rnd)

      // Create a game, and update the expected points for it.
      val game = Helper.getRandomGame(rnd, randomOutcome, team1, team2)
      games.addOne(game)

      randomOutcome match {
        case Outcome.Win => {
          expectedPoints(team1) += Ranker.WinPoints
          expectedPoints(team2) += Ranker.LossPoints
        }
        case Outcome.Loss => {
          expectedPoints(team2) += Ranker.WinPoints
          expectedPoints(team1) += Ranker.LossPoints
        }
        case Outcome.Draw => {
          expectedPoints(team2) += Ranker.DrawPoints
          expectedPoints(team1) += Ranker.DrawPoints
        }
      }
    }

    val points = Ranker.computePointsGames(games.toList)
    assert(points === expectedPoints)
  }

  test("getRanks -> Simple") {
    val points = Map("Pizza" -> 10, "Burger" -> 4, "Taco" -> 25)
    val ranks = Ranker.getRanks(points)

    assert(ranks === List(("Taco", 25), ("Pizza", 10), ("Burger", 4)))
  }

  test("getRanks -> Clash") {
    val points = Map("Pizza" -> 10, "Yogurt" -> 4, "Taco" -> 5, "Kebab" -> 4, "Pie" -> 1)
    val ranks = Ranker.getRanks(points)

    assert(ranks === List(
      ("Pizza", 10),
      ("Taco", 5),
      ("Kebab", 4),
      ("Yogurt", 4),
      ("Pie", 1)))
  }

  test("showRanks") {
    val ranks = List(
      ("Pizza", 10),
      ("Taco", 5),
      ("Kebab", 4),
      ("Yogurt", 4),
      ("Pie", 2),
      ("Candy", 1))
    val output = Ranker.showRanks(ranks)

    assert(output === List(
      "1. Pizza, 10 pts",
      "2. Taco, 5 pts",
      "3. Kebab, 4 pts",
      "3. Yogurt, 4 pts",
      "5. Pie, 2 pts",
      "6. Candy, 1 pt"))
  }
}

object Helper {
  object Outcome extends scala.Enumeration {
    val Win, Loss, Draw = Value
  }

  def getRandomTeams(rnd: Random, teams: List[String]): (String, String) = {
    val selection = Random.shuffle(teams).take(2)
    (selection(0), selection(1))
  }

  def getRandomOutcome(rnd: Random): Outcome.Value = {
    val outcomes = Outcome.values.toList
    rnd.shuffle(outcomes).head
  }

  def getRandomGame(rnd: Random, outcome: Outcome.Value, team1: String, team2: String): Game = {
    // Random scores, first: one between (10,100), and another above it.
    val lossScore = rnd.nextInt(90) + 10
    val winScore = lossScore + 1 + rnd.nextInt(20)

    outcome match {
      case Outcome.Win => Game(team1, winScore, team2, lossScore)
      case Outcome.Loss => Game(team1, lossScore, team2, winScore)
      case Outcome.Draw => Game(team1, winScore, team2, winScore)
    }
  }
}

