## Notes

### Comments
- Commit log will be ad-hoc to "show work" (would squash these normally)

### Running

```
$ sbt --error run < INPUT_FILE > OUTPUT_FILE
```

### Testing

```
$ sbt test
```

